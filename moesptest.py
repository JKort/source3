import pandas as pd
import numpy as np
import math
import operator
import control
import time
import moesp
import ssid
import matplotlib.pyplot as plt
from numpy.lib import scimath
from SIPPY import *
from scipy import linalg
from scipy import optimize
from scipy import signal
from sklearn.model_selection import train_test_split
from control import matlab
import random


start = time.time()
class structtype():
    pass

yy = pd.read_csv("ytest.csv")
y = yy.to_numpy()
y = np.block([[0],[y]])

def systemsim(A, B, C, D, t, u):

    dt = 0.1
    system = (A, B, C, D, dt)

    # tt, y, x = signal.dlsim(system, u)
    u1 = np.array([u])
    # y1 = np.array([y])
    # identified_system = ssid.N4SID(u, y, 3, 98, 2)
    # videntified_system = system_identification(y, u, 'N4SID', SS_fixed_order=2)  #  The MOESP is the problem, it only works for low order values, try the Matlab one
    A_new, B_new, C_new, D_new = moesp.moesp(y, u, 2)
    identified_system = (A_new,B_new,C_new,D_new,y)
    # identified_system.y = y
    return identified_system

System = [structtype() for i in range(10)]
t = np.linspace(0,10,101)
n = 0.2
m = 0.25
u = np.zeros([101,1])

for i in range(len(t)):
    u[i] = 5*np.sin(0.3*t[i])

for k in range(10):
    System[k].a = np.array([[0,1],[ -n*(k+1), -m*(k+1)]])
    System[k].b = np.array([[0],[1]])
    System[k].c = np.array([[1,0]])
    System[k].d = np.array([[0]])
    System[k].u = u
    System[k].sys = control.StateSpace(System[k].a, System[k].b, System[k].c, System[k].d)
    System[k].tf = control.tf(System[k].sys)
    identified = systemsim(System[k].a, System[k].b, System[k].c, System[k].d, t, System[k].u)
    #  System[k].x = identified[-2]
    System[k].y = identified[-1]
    System[k].A = identified[0]
    System[k].B = identified[1]
    System[k].C = identified[2]
    System[k].D = identified[3]
    # System[k].A = identified.A
    # System[k].B = identified.B
    # System[k].C = identified.C
    # System[k].D = identified.D
    # System[k].y = identified.y

    System[k].sys2 = control.StateSpace(System[k].A,System[k].B,System[k].C,System[k].D)
    System[k].newtf = control.tf(System[k].sys2)
    w, v = np.linalg.eig(System[k].a)
    ww, vv = np.linalg.eig(System[k].A)


    print('Eigenvalue org: ' + str(w))
    print('Eigenvalue MOESP: ' + str(ww))

plt.plot(System[0].u)
plt.plot(System[0].y)

plt.show()