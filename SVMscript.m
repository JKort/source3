 clear all; close all; clc


order = 2; %Order of system
in = 1; %Amount of inputs
out = 1; %Amount of outputs
N = 2; %Amount of systems to generate
s = 50; %Amount of different input output pairs per system
m = N*s; %Total amount of systems
u_time = 80; %amount of time instants per system 
t = linspace(0, 5*s, s*u_time); %time
Ts = t(2)-t(1);
uu = 5 * sin(0.3*t) + 2 * sin(2*t + pi()/8); %Input vector
u = reshape(uu, [u_time,s]); %Reshaping th input vector in s different parts
Class = cell(m,1); %initialization vector for the Class
Y = zeros(m,u_time); %intitialization vector for output data
Classes = {'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten'};
correct = 0;
noise = false; 
flt_rate = 0.5;
fault = false;
A = [0 1; -0.5 -0.9];
B= [0; 1];
C = [1 0];
D = 0;
system1 = ss(A,B,C,D);
system2 = c2d(system1,Ts);


if N == 2
    %A random system is created, after which a second system is created by
    %slightly altering the first  system
    while correct == 0
        system(1).ss = system2;
        system(2).ss = system(1).ss;
        system(2).ss.A(2,2) =  system(2).ss.A(2,2)*flt_rate;
        for i = 1:u_time
            Ob(i,:) = system(1).ss.C * system(1).ss.A^(i-1); %Compute observability matrix
            Ob2(i,:) = system(2).ss.C * system(2).ss.A^(i-1);
        end
        Rank1 = rank(Ob); %Compute rank of observability matrix
        Rank2 = rank(Ob2);
        Rank_12 = rank([Ob Ob2]);
        if Rank_12 == Rank1 + Rank2 %Necessary to create correct classifier
            correct = 1;
        end   
    end   
else
    
    %    This can be used to create N completely different systems
    system(1).ss = drss(order,in,out);
    for i = 2:N 
        correct = 0;
        while correct == 0
            system(i).ss = drss(order,in,out); %Simulate a random discrete system
            for j = 1:i-1
                for k = 1:u_time
                    Ob(k,:) = system(j).ss.C * system(j).ss.A^(k-1); %Compute observability matrix
                    Ob2(k,:) = system(i).ss.C * system(i).ss.A^(k-1);
                end
                Rank1 = rank(Ob); %Compute rank of observability matrix
                Rank2 = rank(Ob2);
                Rank_12 = rank([Ob Ob2]);
                correct = 1;
                if Rank_12 ~= Rank1 + Rank2 %Necessary to create correct classifier
                    correct = 0;
                    break
                end   
            end
        end
    end
    
    
end
        
for i = 1:N 
%     if i > 1
%         fault = true
    for j = 1:s
        M = (i-1)*s+j;
        sys(M).ss = system(i).ss; 
        sys(M).input_number = j;
        sys(M).u = u(:,j);
        [sys(M).y, sys(M).x] = dlsim(sys(M).ss.A, sys(M).ss.B, sys(M).ss.C, sys(M).ss.D, sys(M).u); %simulate output y for s different inputs
        if fault %Insert faulty systems
            sys(M).ss.A(2,2) = sys(M).ss.A(2,2)*flt_rate ;
        end
        if noise % Add noise
            eta = 0.009*mean(abs(sys(M).y))*randn(length(sys(M).y),1);
            y1 = sys(M).y;
            sys(M).y = sys(M).y + eta;
            y2 = sys(M).y;
            noiseratio = norm(y1)/norm(eta);
            if noiseratio < 100
               error('Too much noise, try again')
            end         
        end
        
        
        Y(M,:) = sys(M).y';%insert the input into the SVM array
        Class{M} = Classes{i}; %Assign the class
    end
end

plot(sys(4).u)
hold on
plot(sys(4).y)
plot(sys(s+4).y)
legend('u','y1','y2')


systemdb = array2table(Y);
systemdb.Class = Class;
%After this, open classification learner
classificationLearner

