% In this script the BC kernels are calculated

% BC Determinant kernel
M=zeros(2,2);
lambda = 0.11;


for i = 1:80
    P = lambda^i * (A1^i)' * C1' * C2 * A2^i;
    M = M + P;
end

kd = det(M)