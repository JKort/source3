% KNN
function [classified_class] = knn(metric, trainingset, test, k )

global LAMBDA_BC
global alpha
global beta
global lambda_MMAC

    for i = 1:length(trainingset)
        % trainingset_loc = i
        if strcmp('Martin Distance',metric)
           dist = Martin(test.A, trainingset(i).A, test.C, trainingset(i).C);
        elseif strcmp('nugap', metric)
            sys1 = ss(test.A, test.B, test.C, test.D);
            sys2 = ss(trainingset(i).A, trainingset(i).B, trainingset(i).C, trainingset(i).D);
            dist = nugap(sys1,sys2);
        elseif strcmp('BC kernel', metric)
            dist = BCdet(test.A,trainingset(i).A,test.C,trainingset(i).C,LAMBDA_BC);
        elseif strcmp('MMAC', metric)
            dist = MMAC(trainingset(i), test, alpha, beta, lambda_MMAC);
        else
           error('Incorrect distance metric specified. Choose "Martin Distance", "BC kernel", "nugap" or "MMAC" ')
        end
        
        distances(i) = abs(dist);
    end
    if strcmp('BC kernel',metric)
        [B, I] = sort(distances,'descend');
    else
        [B, I] = sort(distances);
    end
    
    votes = [];
    for i = 1:k
        response = trainingset(I(i)).Class ;
        if i >= 2
            [lia, location] = ismember(response, votes(:,1));
             if lia == 1
             votes(location,2) = votes(location,2)+1;
            else
            votes(end+1,:) = [response, 1];
             end
        else
        votes(end+1,:) = [response, 1];     
        end
    end
    sortvotes = sortrows(votes,2,'descend');
    classified_class = sortvotes(1,1);
end