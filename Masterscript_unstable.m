 clear all; close all; clc

global LAMBDA_BC
global alpha
global beta
global lambda_MMAC

order = 2; %Order of system
in = 1; %Amount of inputs
out = 1; %Amount of outputs
NN = [10 20 50];
s = 10; %Amount of different input output pairs per system
k = 3; %Amount of neighbors
gr = 5; %Amount of groups for testing and training
LAMBDA_BC = 0.8; % Value for BC
lambda_MMAC = 1;
alpha = 1;
beta = 1;
a1 = [3 6 9 12 15];
a2 = [2 5 8 11 14 4 7 10 13];
N_counter = 0;
u_time = 100;
t = linspace(0,5*s,s*u_time);
Ts = t(2)-t(1);
uu = 5 * sin(0.3*t) + 2 * sin(2*t + pi()/8); %Input vector
u = reshape(uu, [u_time,s]); 
    
for instance = 1:50       
for N = 10
    
    m = N*s;%Total amount of systems

    
    N_counter = N_counter + 1    ;
    X = 1:m; %To use later as a location column for the generated systems

for i = 1:N
    
    loc1 = mod(i,5)+1;
    loc2 = floor((i-1)/5)+1;
    
    A = [0 1; a1(loc1) a2(loc2)];
    B = [0; 1];
    C = [1 0];
    D = 0;    
    system1 = ss(A,B,C,D);
    system(i).ss = c2d(system1,Ts);
    
    for j = 1:s
        M = (i-1)*s+j;
        sys(M).ss = system(i).ss;
        sys(M).Class = i;
        sys(M).input_number = j;
        sys(M).u = u(:,j);
        [sys(M).y, sys(M).x] = dlsim(sys(M).ss.A, sys(M).ss.B, sys(M).ss.C, sys(M).ss.D, sys(M).u); %simulate output y for s different inputs
    end
end

divider = round(s/gr);
X1=[];X2=[];X3=[];X4=[];X5=[];

for i = 1:N
    X = ((i-1)*s)+randperm(s);
    X1 = [X1 X(1:divider)];
    X2 = [X2 X(divider+1:2*divider)];
    X3 = [X3 X(2*divider+1:3*divider)];
    X4 = [X4 X(3*divider+1:4*divider)];
    X5 = [X5 X(4*divider+1:end)];
end

%% Test 
for metric_counter = 1:4
    if metric_counter == 1
        metric = 'Martin Distance';
    elseif metric_counter == 2
        metric = 'BC kernel';
    elseif metric_counter == 3
        metric = 'nugap';
    else
        metric = 'MMAC' ;
    end
    
    if metric_counter <= 3
    for i = 1:m
       [sys(i).A, sys(i).B, sys(i).C, sys(i).D] = systemsim(sys(i).u, sys(i).y);
    end
    end
    
    accuracy = zeros(1,5); timed = zeros(1,5);
    
    for j = 1:gr
        tic
        if j == 1
            clear testset trainingset
            trainingset = sys([X2 X3 X4 X5]);
            testset = sys(X1);
        elseif j == 2
            trainingset = sys([X1 X3 X4 X5]);
            testset = sys(X2);
        elseif j == 3
            trainingset = sys([X1 X2 X4 X5]);
            testset = sys(X3);
        elseif j == 4
            trainingset = sys([X1 X2 X3 X5]);
            testset = sys(X4);
        elseif j == 5
            trainingset = sys([X1 X2 X3 X5]);
            testset = sys(X5);
        else
            error('Range not specified correctly')
        end

        results = zeros(1,length(testset));
        correct = 0;

        for i = 1:length(testset)
            % i
            test = testset(i);
            results(i) = knn(metric, trainingset, test, k);
            if results(i) == test.Class
                correct = correct+1;
            end
        end
        accuracy(j) = correct/length(testset) * 100;
        timed(j) = toc;
    end

    loc = find(N==NN);
    accuracy_matrix1(metric_counter, loc) = mean(accuracy);
    time_matrix1(metric_counter, loc) = sum(timed);
    
    if instance == 1
        total_mean_accuracy(metric_counter, loc)  = accuracy_matrix1(metric_counter, loc);
        total_mean_time(metric_counter, loc) = time_matrix1(metric_counter, loc);
    else
        total_mean_accuracy(metric_counter, loc)  = mean([total_mean_accuracy(metric_counter, loc) accuracy_matrix1(metric_counter, loc)]);
        total_mean_time(metric_counter, loc) = mean([total_mean_time(metric_counter, loc) time_matrix1(metric_counter, loc)]);
    end

end
end
end

% Plot accuracy
barplot10 = accuracy_matrix1;
barplottime = time_matrix1;

figure(1)
subplot(2,1,1)
title('Accuracy per metric')
bar(barplot10)
set(gca,'xticklabel',{'MD';'BC';'nu-gap';'MMAC'})
ylabel('Accuracy (%)')
subplot(2,1,2)
title('Computational time needed')
bar(barplottime)
set(gca,'xticklabel',{'MD';'BC';'nu-gap';'MMAC'})
ylabel('Time (s)')

save('scenario2b.mat')
% 
% figure(1)
% title('Accuracy for different amount of classes')
% subplot(3,1,1)
% bar(barplot10)
% set(gca,'xticklabel',{'MD';'BC';'nu-gap'})
% ylabel('Accuracy (%)')
% subplot(3,1,2)
% bar(barplot20)
% set(gca,'xticklabel',{'MD';'BC';'nu-gap'})
% ylabel('Accuracy (%)')
% title('20 classes')
% subplot(3,1,3)
% bar(barplot50)
% set(gca,'xticklabel',{'MD';'BC';'nu-gap'})
% ylabel('Accuracy (%)')
% title('50 classes')
% % subplot(4,1,4)
% % bar(barplot100)
% % set(gca,'xticklabel',{'MD';'BC';'nu-gap'})
% % ylabel('Accuracy (%)')
% % title('100 classes')
% 
% % Plot timed
% barplot10 = time_matrix1;
% barplot20 = time_matrix2;
% barplot50 = time_matrix3;
% barplot100 = time_matrix4;
% 
% figure(2)
% title('Accuracy for different amount of classes')
% subplot(3,1,1)
% bar(barplot10)
% set(gca, 'xticklabel',{'MD';'BC';'nu-gap'}) 
% ylabel('Computational time (s)')
% title('10 classes')
% subplot(3,1,2)
% bar(barplot20)
% set(gca,'xticklabel',{'MD';'BC';'nu-gap'})
% ylabel('Computational time (s)')
% title('20 classes')
% subplot(3,1,3)
% bar(barplot50)
% set(gca,'xticklabel',{'MD';'BC';'nu-gap'})
% ylabel('Computational time (s)')
% title('50 classes')
% % subplot(4,1,4)
% % bar(barplot100)
% % set(gca,'xticklabel',{'MD';'BC';'nu-gap'})
% % ylabel('Computational time (s)')
% % title('100 classes')