% All systems
close all
clear all

N = 1000;
S = 5;
sub = 10;
m = S*sub;
Class = 1:S;
array = zeros(N/sub+2,m);
n = 1;
nn=0.2;
mm = 0.25;
%10 systems are created each conisting of 10 different (u,y) samples. The
%systems have been created using random values for the a matrix.
for i = 1:S
    a = [0 1; -nn*i -mm*i];
    u = randn(1,N);%Random input signal
    for j = 1:sub
        k=(i-1)*sub+j;
        System(k).Class = Class(i);
        System(k).a = a;
        System(k).b = [0 ; 1];
        System(k).c = [0.5 0.5];
        System(k).d = 0;
        System(k).u = u((N/sub*(j-1)+1):(N/sub*j)); %dividing the input signal into 'sub' equal parts
        [System(k).A, System(k).B, System(k).C, System(k).D, System(k).y] = systemsim(System(k).a,System(k).b,System(k).c,System(k).d,System(k).u);
        System(k).eig = eig(System(k).A);
%         array(:,2*k-1:2*k)= [k k; System(k).Class System(k).Class ;System(k).u System(k).y];
    end
end

% csvwrite('systems1.csv',array)
%% Create KNN algorithm


%%  Calculate Martin Distance between all systems, gives positive values
for i = 1:m
    for j = 1:m 
        [System(i).Martin_Distance(j)] = Martin(System(i).A,System(j).A,System(i).C,System(j).C);
    end
end

figure(1)
plot(System(1).Martin_Distance) %Plot of the distances to system 1
hold on
plot(System(21).Martin_Distance)

%% Calculate BC determinant kernels, positive definite
lambda = 1;

for i = 1:m
    for j = 1:m
        [System(i).BC_det(j)] = BCdet(System(i).A,System(j).A,System(i).C,System(j).C,lambda);
    end
end

figure(2)
plot(System(1).BC_det) %Plot of the distances to system 1
hold on
plot(System(21).BC_det)

figure(3)
plot(abs(System(1).BC_det-1))
hold on
plot(abs(System(21).BC_det-1))
% %% Calculate BC trace kernels
% 
% for i = 1:m
%     for j = 1:m
%         [System(i).BC_trace(j),System(i).BC_trace_norm(j)] = BCtrace(System(i).A,System(j).A,System(i).C,System(j).C,lambda);
%     end
% end
% 
% figure(3)
% plot(System(1).BC_trace_norm)
% hold on
% plot(System(9).BC_trace_norm)
% plot(System(1).BC_det)
% plot(System(9).BC_det)
% legend('trace1','trace9','det1','det9')
