
import pandas as pd

#Read in data using pandas
df = pd.read_csv('systems1.csv')

#create data frame with all training data except the target column
X = df.drop(columns=['Systems'])

y = df['Systems'].values

from sklearn.model_selection import train_test_split

#split dataset into train and test data
X_train, X_test, y_train, y_test = train_test_split(X, y,
test_size=0.2, random_state=1, stratify=y)

from sklearn.neighbors import KNeighborsClassifier

#Create classifier
knn = KNeighborsClassifier(n_neighbors=3)

#Fit classifier to data
knn.fit(X_train,y_train)


#check accuracy of our model on the test data
print('Score of knn: {}'.format(knn.score(X_test, y_test)))

from sklearn.model_selection import cross_val_score
import numpy as np

knn_cv = KNeighborsClassifier(n_neighbors=3)

cv_scores = cross_val_score(knn_cv, X, y, cv=5)

print("Scores after cross validation and mean score: {}".format(cv_scores))
print('cv_scores mean: {}'.format(np.mean(cv_scores)))

from sklearn.model_selection import GridSearchCV

#New knn model, hypertuned using Gridsearch
knn2 = KNeighborsClassifier()

#dictionary of all valyes we want to test for n_neighbors
param_grid = {'n_neighbors': np.arange(1, 25)}

knn_gscv = GridSearchCV(knn2, param_grid, cv=5)

knn_gscv.fit(X, y)

print("Optimal value of {}".format(knn_gscv.best_params_))

print("Best score of GridSearchCV: {}".format(knn_gscv.best_score_))

