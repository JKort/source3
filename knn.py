import pandas as pd
import numpy as np
import math
import operator
import control
import time
import ssid
import matplotlib.pyplot as plt
from numpy.lib import scimath
from SIPPY import *
from scipy import linalg
from scipy import optimize
from scipy import signal
from sklearn.model_selection import train_test_split
from control import matlab
import moesp

start = time.time()
class structtype():
    pass


# initialize values
amount_of_classes = 10
amount_of_generations_per_class = 10

neighbors_count = 5  # Amount of neighbors to consider
metric = 'BC kernel'  # metric = input('Choose distance metric: ') # 'Martin Distance', 'BC kernel', 'nu-gap', 'MMAC'
solver = 'knn'  # 'knn' or 'SVM'
# Value needed for BC kernel
lamda = 0.7
# Values needed for MMAC
alpha = 1
beta = 1
lam_bda = 1


# x = pd.read_csv("systems1.csv")
# print(x.head())


def systemsim(A, B, C, D, t, u):

    dt = 0.1
    system = (A, B, C, D, dt)
    tt, y, x = signal.dlsim(system, u)
    u1 = np.array([u])
    # y1 = np.array([y])
    #identified_system = ssid.N4SID(u1, y, 3, 98, 2)
    identified_system = system_identification(y, u, 'MOESP', SS_fixed_order=2)  #  The MOESP is not the problem, it probably has limited stability
    identified_system.x = x
    identified_system.y = y
    return identified_system

def mmac_distance(Atraining, u, x): # TODO: Aanpassen integraal
    v = np.zeros((len(t), 2))
    B = trainingSet[i].B_org
    C = trainingSet[i].C_org
    BB = np.array([[1, 0], [0, 1]])
    DD = np.array([[0, 0]])
    Am = np.array([[0, 1], [-4, -0.25]])
    # integral = []

    for time in range(len(t)):
        xx = np.array([x[time, :]])
        vv = (Atraining - Am) @ xx.T + B * u[time]
        v[time, :] = vv.T

    sys1 = (Am, BB, C, DD, 0.1)
    t_calc, y_calc, xdash = signal.dlsim(sys1, v, t)  # Simulate x^
    # y_calc, t_calc, xdash = matlab.lsim(sys1, v, t)  # Simulate x^
    e = xdash-x  # error vector 101x2
    cost = []
    err = []

    for time in range(len(t)-1):
        err = []
        # err_init = np.array([e[0]])
        # err = np.array([e[time]])
        past_estimation_errors = []
        for value in range(np.minimum(time+1, 10)):
            if value == 0:
                err = np.array([e[np.maximum(time-9, 0)]])
            else:
                err = np.block([[err], [e[time-min(9,time)+value].T]])

        for value in range(np.minimum(time+1, 10)):
            new_past = np.exp(-lam_bda * -value) * err[min(9, time)-value, :].T @ err[min(9, time)-value, :]
            past_estimation_errors = np.append(past_estimation_errors, new_past)

        newcost = alpha * e[time, :] @ e[time, :].T + beta*sum(past_estimation_errors)
        cost = np.append(cost, newcost)
        # new = 1/lam_bda * ((err[0][0]**2)+(err[0][1]**2)) - 1/lam_bda**2 * (err[0][0]+err[0][1])+2/lam_bda**3\
        #     - np.exp(-lam_bda*t[time])/lam_bda * (err_init[0][0]**2 + err_init[0][1]**2) +\
        #       np.exp(-lam_bda*t[time])/(lam_bda**2) * (err_init[0][0] + err_init[0][1]) - 2/lam_bda**3 * np.exp(-lam_bda*t[time])
        # integral = np.append(integral, new)
        # cost = np.append(cost, alpha * err @ err.T + beta*integral[time])
    return np.average(cost)

def nugap(A1, A2, B1, B2, C1, C2, D1, D2):    # To compute the nugap, the transfer functions of both systems are needed. TODO: test if v-gap works
    P1 = np.block([[A1, B1], [C1, D1]])
    P2 = np.block([[A2, B2], [C2, D2]])
    conjP1 = P1.conj().transpose()
    conjP2 = P2.conj().transpose()
    eye = np.eye(P1.shape[0], P1.shape[1])
    psi = (1/scimath.sqrt(eye + P2 * conjP2)) * (P1-P2) * (1/scimath.sqrt(eye + conjP1 * P1))
    nugap_distance = np.linalg.norm(psi, np.inf)
    return nugap_distance

    # Find the norm, look up norm function for python
    # cepstral distance (data driven Martin)

def BC(A1, A2, C1, C2, lamda):
    C = np.transpose(C1) * C2
    C_1 = np.transpose(C1) * C1
    C_2 = np.transpose(C2) * C2
    A = lamda * np.transpose(A1)
    A_2 = lamda * np.transpose(A2)

    # The Lyapunov equation Q = lambda*A1'*Q*A2 + C1'*C2 is solved
    Q12 = control.dlyap(A, np.transpose(A2), C)
    M1 = control.dlyap(A,np.transpose(A1),C_1)
    M2 = control.dlyap(A_2, np.transpose(A2), C_2)
    M1M1 = np.linalg.det(M1)**2
    M2M2 = np.linalg.det(M2)**2
    BC_Det_Kernel = np.linalg.det(Q12)**2
    return BC_Det_Kernel / (np.sqrt(M1M1)*np.sqrt(M2M2))


def Martin(A1, A2, C1, C2): #A1 and A2 need to be 2x2 matrices and C1 and C2 should be 1x2
    ZERO = np.zeros((2,2))
    AA = np.block([[np.transpose(A1), ZERO ],[ZERO, np.transpose(A2)]])
    CC = np.dot(np.block([[np.transpose(C1)],[np.transpose(C2)]]),np.block([C1, C2]))


    Q = control.dlyap(AA, CC)
    Q11 = Q[0:2,0:2]
    Q12 = Q[0:2,2:]
    Q21 = Q[2:,0:2]
    Q22 = Q[2:,2:]

    inv_Q11 = np.linalg.inv(Q11)
    inv_Q22 = np.linalg.inv(Q22)

    matrix = np.block([[ZERO, np.dot(inv_Q11,Q12)], [np.dot(inv_Q22,Q21), ZERO]])
    lamda, nothing = np.linalg.eig(matrix)
    lamda = np.sort(lamda)

    lambda1sq = lamda[-1]**2
    lambda2sq = lamda[-2]**2

    #The Martin distance is calculated according to the formula: d(M1,M2)^2 = -ln(product(cos(phi(i)) ^ 2)) for i=1 to n( in this case 2)
    Martin_Distance = -np.log(lambda1sq*lambda2sq)
    return Martin_Distance


def knn(trainingSet, test, k):  # here we are defining our model

    distances = {}

    for i in range(len(trainingSet)):
        if metric == 'Martin Distance':
            dist = Martin(test.A, trainingSet[i].A, test.C, trainingSet[i].C)
            reverse = False
        elif metric == 'BC kernel':
            dist = BC(test.A, trainingSet[i].A, test.C, trainingSet[i].C, lamda)
            reverse = True
        elif metric == 'nu-gap':
            dist = nugap(test.A, trainingSet[i].A, test.B, trainingSet[i].B, test.C, trainingSet[i].C, test.D, trainingSet[i].D)
            reverse = False
        elif metric == 'MMAC':
            dist = mmac_distance(trainingSet[i].A_org, test.u, test.x)
            reverse = False
        else:
            raise Exception('metric not specified correctly. Choose Martin Distance, BC kernel, nu-gap or MMAC')
        distances[i] = abs(dist)
    sortdist = sorted(distances.items(), key=operator.itemgetter(1), reverse=reverse)
    neighbors = []
    for i in range(k):
        neighbors.append(sortdist[i][0])
    votes = {}  # to get most frequent class of rows
    for i in range(len(neighbors)):
        response = trainingSet[neighbors[i]].Class
        if response in votes:
            votes[response] += 1
        else:
            votes[response] = 1
    sortvotes = sorted(votes.items(), key=operator.itemgetter(1), reverse=True)
    return sortvotes[0][0], neighbors


total_number_of_systems = amount_of_classes*amount_of_generations_per_class
System = [structtype() for i in range(total_number_of_systems)]

t = np.linspace(0, 10, 101)
X = np.ones([total_number_of_systems, 1])
Y = np.ones([total_number_of_systems, 1])

# A_1 = [-1, -2, -3, -4, -5]
# A_2 = [-1, -2, -3, -4, -5]
A_1 = [-0.2, -0.3, -0.4, -0.6, -0.8]
A_2 = [-0.2, -0.4, -0.5, -0.7, -0.8]
u_a = [1, 2, 3, 4, 5]
u_b = [0.3, 0.6, 1, 2, 4, 0.3]


for k in range(amount_of_classes):
    for j in range(amount_of_generations_per_class):
        i = 10 * k + j
        System[i].Class = k
        System[i].u = u_a[int(j/2)] * np.sin(u_b[int(np.ceil(j/2))] * t) + 2 * np.sin(2 * t + np.pi / 8)
        System[i].A_org = np.array([[0, 1], [A_1[int(np.floor(k)/5)], A_2[int(k-np.floor(k/5)*5)]]])
        System[i].B_org = np.array([[0], [1]])
        System[i].C_org = np.array([[1, 0]])
        System[i].D_org = np.array([[0]])
        identified = systemsim(System[i].A_org, System[i].B_org, System[i].C_org, System[i].D_org, t, System[i].u)
        System[i].x = identified.x
        System[i].y = identified.y
        System[i].A = identified.A
        System[i].B = identified.B
        System[i].C = identified.C
        System[i].D = identified.D
        System[i].tf = control.ss2tf(System[i].A, System[i].B, System[i].C, System[i].D)
        X[i] = i
        Y[i] = System[i].Class

X_rand = np.zeros([total_number_of_systems, 1])
y_rand = np.zeros([total_number_of_systems, 1])

divider = int(total_number_of_systems/5)

# split dataset into train and test data
X_train, X_rand[0:divider], y_train, y_rand[0:divider] = train_test_split(X, Y, test_size=0.2, random_state=1, stratify=Y)

X_train1, X_rand[divider:2*divider], y_train1, y_rand[divider:2*divider] = train_test_split(X_train, y_train, test_size=0.25, random_state=1, stratify=y_train)

X_train2, X_rand[2*divider:3*divider], y_train2, y_rand[2*divider:3*divider] = train_test_split(X_train1, y_train1, test_size=(1/3), random_state=1, stratify=y_train1)

X_rand[4*divider:5*divider], X_rand[3*divider:4*divider], y_rand[4*divider:5*divider], y_rand[3*divider:4*divider] = train_test_split(X_train2, y_train2, test_size=0.5, random_state=1, stratify=y_train2)

accuracy = []
trainingSet = [structtype() for j in range(len(X_train))]
testSet = [structtype() for h in range(len(X_rand[0:divider]))]

del X_train, y_train
for j in range(5):
    if j == 0:
        X_train = X_rand[divider:]
        X_test = X_rand[:divider]
    elif j == 1:
        X_train[0:divider] = X_rand[0:divider]
        X_train[divider:4*divider] = X_rand[2*divider:5*divider]
        X_test = X_rand[divider:2*divider]
    elif j == 2:
        X_train[0:2*divider] = X_rand[0:2*divider]
        X_train[2*divider:4*divider] = X_rand[3*divider:5*divider]
        X_test = X_rand[2*divider:3*divider]
    elif j == 3:
        X_train[0:3*divider] = X_rand[0:3*divider]
        X_train[3*divider:4*divider] = X_rand[4*divider:5*divider]
        X_test = X_rand[3*divider:4*divider]
    elif j == 4:
        X_train = X_rand[:4*divider]
        X_test = X_rand[4*divider:]
    else:
        print('Range not specified correctly')

    results = []
    correct = 0

    for i in range(len(X_train)):
        trainingSet[i] = System[int(X_train[i][0])]

    for l in range(len(X_test)):
        testSet[l] = System[int(X_test[l][0])]
        test = testSet[l]
        result, neigh = knn(trainingSet, test, neighbors_count)
       # print(result)
        results = np.append(results, result)
        if result == testSet[l].Class:
            correct += 1
        # print("This sample belongs to class: " + str(results[i]))
    accur = correct/len(X_test) * 100
    accuracy = np.append(accuracy, accur)

end = time.time()
time_taken = end - start

print('The accuracy is: ' + str(accuracy.mean()) + '%')
print('The time taken is: ' + '%.2f' % time_taken + ' seconds')

