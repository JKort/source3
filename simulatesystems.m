function [A1, B1, C1, D1] = systemsim(a1,b1,c1,d1,u1)

% e = -.25;
% f = -.8;
% g = -.6;
% h = -.25;
% 
% a1= [0 1; e f];
% b1= [0; 1];
% c1= [1 0];
% d1= 0;

% a2= [0 1;g h];
% b2= [0; 1];
% c2= [1 0];
% d2= 0;

N = 500;
u1 = randn(N,1);
u2 = randn(N,1);

y1 = dlsim(a1,b1,c1,d1,u1);

d = 6;

[ss1,ssfun1] = moesp(y1,u1,d);

% bar(ss2)

[A1,B1,C1,D1] = ssfun1(2);

% %    To compare with the actual system, we use the bode diagram:
%     w = [0:0.005:0.5]*(2*pi); 		% Frequency vector
%     m1 = dbode(a1,b1,c1,d1,1,1,w);
% %     m2 = dbode(a1,b1,c1,d1,1,2,w);
%     M1 = dbode(A1,B1,C1,D1,1,1,w);
% %    M2 = dbode(A1,B1,C1,D1,1,2,w);

% Plot comparison
%     figure(1)
%     hold off;subplot;
%     title('System 1')
%     subplot(221);plot(w/(2*pi),[m1(:,1),M1(:,1)]);title('Input 1 -> Output 1');
 %   subplot(222);plot(w/(2*pi),[m2(:,1),M2(:,1)]);title('Input 2 -> Output 1');
%   subplot(223);plot(w/(2*pi),[m1(:,2),M1(:,2)]);title('Input 1 -> Output 2');
%    subplot(224);plot(w/(2*pi),[m2(:,2),M2(:,2)]);title('Input 2 -> Output 2');
%}
% 
%      w = [0:0.005:0.5]*(2*pi); 		% Frequency vector
%     m1 = dbode(a2,b2,c2,d2,1,1,w);
% %    m2 = dbode(a2,b2,c2,d2,1,2,w);
%     M1 = dbode(A2,B2,C2,D2,1,1,w);
% %    M2 = dbode(A2,B2,C2,D2,1,2,w);

% Plot comparison
%     figure(2)
%     hold off;subplot;
%     title('System 2')
%     subplot(221);plot(w/(2*pi),[m1(:,1),M1(:,1)]);title('Input 1 -> Output 1');
%     subplot(222);plot(w/(2*pi),[m2(:,1),M2(:,1)]);title('Input 2 -> Output 1');
%     subplot(223);plot(w/(2*pi),[m1(:,2),M1(:,2)]);title('Input 1 -> Output 2');
%     subplot(224);plot(w/(2*pi),[m2(:,2),M2(:,2)]);title('Input 2 -> Output 2');

eiga1 = eig(a1);
eigA1 = eig(A1);
eiga2 = eig(a2);
eigA2 = eig(A2);
    
    
fprintf('eigenvalues of a1 are %.15g %.15g \n', eiga1(1),eiga1(2));
fprintf('eigenvalues of A1 are %.15g %.15g \n', eigA1(1),eigA1(2));
fprintf('eigenvalues of a2 are %.15g %.15g \n', eiga2(1),eiga2(2));
fprintf('eigenvalues of A2 are %.15g %.15g \n', eigA2(1),eigA2(2));





