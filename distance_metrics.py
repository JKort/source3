import numpy as np
import control
from scipy import signal
from numpy.lib import scimath

def mmac_distance(Atraining, u, x):
    v = np.zeros((len(t), 2))
    B = trainingSet[i].B_org
    C = trainingSet[i].C_org
    BB = np.array([[1, 0], [0, 1]])
    DD = np.array([[0, 0]])
    Am = np.array([[0, 1], [-4, -0.25]])
    # integral = []

    for time in range(len(t)):
        xx = np.array([x[time, :]])
        vv = (Atraining - Am) @ xx.T + B * u[time]
        v[time, :] = vv.T

    sys1 = (Am, BB, C, DD, 0.1)
    t_calc, y_calc, xdash = signal.dlsim(sys1, v, t)  # Simulate x^
    # y_calc, t_calc, xdash = matlab.lsim(sys1, v, t)  # Simulate x^
    e = xdash-x  # error vector 101x2
    cost = []
    err = []

    for time in range(len(t)-1):
        err = []
        # err_init = np.array([e[0]])
        # err = np.array([e[time]])
        past_estimation_errors = []
        for value in range(np.minimum(time+1, 10)):
            if value == 0:
                err = np.array([e[np.maximum(time-9, 0)]])
            else:
                err = np.block([[err], [e[time-min(9,time)+value].T]])

        for value in range(np.minimum(time+1, 10)):
            new_past = np.exp(-lam_bda * -value) * err[min(9, time)-value, :].T @ err[min(9, time)-value, :]
            past_estimation_errors = np.append(past_estimation_errors, new_past)

        newcost = alpha * e[time, :] @ e[time, :].T + beta*sum(past_estimation_errors)
        cost = np.append(cost, newcost)
        # new = 1/lam_bda * ((err[0][0]**2)+(err[0][1]**2)) - 1/lam_bda**2 * (err[0][0]+err[0][1])+2/lam_bda**3\
        #     - np.exp(-lam_bda*t[time])/lam_bda * (err_init[0][0]**2 + err_init[0][1]**2) +\
        #       np.exp(-lam_bda*t[time])/(lam_bda**2) * (err_init[0][0] + err_init[0][1]) - 2/lam_bda**3 * np.exp(-lam_bda*t[time])
        # integral = np.append(integral, new)
        # cost = np.append(cost, alpha * err @ err.T + beta*integral[time])
    return np.average(cost)

def nugap(A1, A2, B1, B2, C1, C2, D1, D2):    # To compute the nugap, the transfer functions of both systems are needed. TODO: test if v-gap works
    P1 = np.block([[A1, B1], [C1, D1]])
    P2 = np.block([[A2, B2], [C2, D2]])
    conjP1 = P1.conj().transpose()
    conjP2 = P2.conj().transpose()
    eye = np.eye(P1.shape[0], P1.shape[1])
    psi = (1/scimath.sqrt(eye + P2 * conjP2)) * (P1-P2) * (1/scimath.sqrt(eye + conjP1 * P1))
    nugap_distance = np.linalg.norm(psi, np.inf)
    return nugap_distance

    # Find the norm, look up norm function for python
    # cepstral distance (data driven Martin)

def BC(A1, A2, C1, C2, lamda):
    C = np.transpose(C1) * C2
    C_1 = np.transpose(C1) * C1
    C_2 = np.transpose(C2) * C2
    A = lamda * np.transpose(A1)
    A_2 = lamda * np.transpose(A2)

    # The Lyapunov equation Q = lambda*A1'*Q*A2 + C1'*C2 is solved
    Q12 = control.dlyap(A, np.transpose(A2), C)
    M1 = control.dlyap(A,np.transpose(A1),C_1)
    M2 = control.dlyap(A_2, np.transpose(A2), C_2)
    M1M1 = np.linalg.det(M1)**2
    M2M2 = np.linalg.det(M2)**2
    BC_Det_Kernel = np.linalg.det(Q12)**2
    return BC_Det_Kernel / (np.sqrt(M1M1)*np.sqrt(M2M2))


def Martin(A1, A2, C1, C2): #A1 and A2 need to be 2x2 matrices and C1 and C2 should be 1x2
    ZERO = np.zeros((2,2))
    AA = np.block([[np.transpose(A1), ZERO ],[ZERO, np.transpose(A2)]])
    CC = np.dot(np.block([[np.transpose(C1)],[np.transpose(C2)]]),np.block([C1, C2]))


    Q = control.dlyap(AA, CC)
    Q11 = Q[0:2,0:2]
    Q12 = Q[0:2,2:]
    Q21 = Q[2:,0:2]
    Q22 = Q[2:,2:]

    inv_Q11 = np.linalg.inv(Q11)
    inv_Q22 = np.linalg.inv(Q22)

    matrix = np.block([[ZERO, np.dot(inv_Q11,Q12)], [np.dot(inv_Q22,Q21), ZERO]])
    lamda, nothing = np.linalg.eig(matrix)
    lamda = np.sort(lamda)

    lambda1sq = lamda[-1]**2
    lambda2sq = lamda[-2]**2

    #The Martin distance is calculated according to the formula: d(M1,M2)^2 = -ln(product(cos(phi(i)) ^ 2)) for i=1 to n( in this case 2)
    Martin_Distance = -np.log(lambda1sq*lambda2sq)
    return Martin_Distance
