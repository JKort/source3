function [Martin_Distance] = Martin(A1, A2, C1, C2) % Calculation of the Martin Distance



%First the A and C matrices are set up
ZERO = zeros(size(A1,1),size(A1,2));
AA = [A1' ZERO;
      ZERO  A2'];
CC = [C1'; C2']*[C1 C2];  
% eigtest = eig(AA);
%The Lyapunov equation Q = AA'*Q*AA + CC'*CC is solved
Q = dlyap(AA,CC);

Q11 = Q(1:2,1:2); 
Q12 = Q(1:2,3:4);
Q21 = Q(3:4,1:2);
Q22 = Q(3:4,3:4);


invQ11 = inv(Q11);
invQ22 = inv(Q22);

matrix = [ZERO  invQ11*Q12;
          invQ22*Q21  ZERO];
%computation of eigenvalues, n largest eigenvalues are used
lambda = eig(matrix);

% phi1 = acos(lambda(1));
% phi2 = acos(lambda(2));
lambda1sq = lambda(1)^2;
lambda2sq = lambda(2)^2;

%The Martin distance is calculated according to the formula: 
% d(M1,M2)^2 =-ln ( product(cos(phi(i))^2) ) for i=1 to n(in this case 2)
Martin_Distance = -log(prod([lambda1sq lambda2sq]));



