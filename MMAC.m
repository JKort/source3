function [average_cost] = MMAC(training, test, alpha, beta, lambda)

% A = [0 1; -0.5 -0.9];
% B= [0; 1];
% C = [1 0];
% D = 0;
% system1 = ss(A,B,C,D);
% system2 = c2d(system1,Ts);

A = training.ss.A;
B = training.ss.B;
C = training.ss.C;
BB = [1 0;0 1];
DD = [0 0];
Am = [0.56 0.4;-1.6 0.46];

x = test.x;
u = test.u;
% y = test.y;
xdash(1,:) = [0 0];

for i = 1:length(u)
   v(i,:) =  ( (A-Am) * x(i,:)' + B * u(i))';
   %xdashdot(i,:) = (Am*xdash(i,:)' + (A-Am) * x(i,:)' + B * u(i))';
   %xdash(i+1,:) = 0.06006 * xdashdot(i,:) + xdash(i,:);
end


[ydash, xdash] = dlsim(Am, BB, C, DD, v);
e = xdash-x;


% for i = 1:length(u)
%     err = [];
%     %past_estimation_errors = [];
%     past_est_error = [];
%     
%     for j = 1:min(i,10)
%         err = [err ; e(i-min(10,i)+j, :)];
%     end
%     for j = 1:min(i,10)
%        past_est_error(j) = err(min(10,i)-j+1, :) * err(min(10,i)-j+1, :)';
%        % past_estimation_errors(j) = exp(-lambda * (j-1)) * err(min(10,i)-j+1, :) * err(min(10,i)-j+1, :)';
%     end
%     cost(i) = sum(past_est_error);
%     %cost(i) = alpha * e(i,:) * e(i,:)' + beta * sum(past_estimation_errors);
% end


for i = 30
   
   past_estimation_errors = [];
   
   for j = 0:10
      past_estimation_errors(j+1) = exp(-lambda * j) * e(i-j, :) * e(i-j, :)' ;
   end
   cost(30) = sum(past_estimation_errors); 
   
end

average_cost = cost(30);
end