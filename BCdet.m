% In this function the BC kernels are calculated
function [BCnormalized] = BCdet(A1,A2,C1,C2,lambda)
% BC Determinant kernel
C = C1'*C2;
C_1 = C1'*C1;
C_2 = C2'*C2;

A = lambda*A1';
A_2 = lambda*A2';
B = A2;

%The Lyapunov equation Q = lambda*A1'*Q*A2 + C1'*C2 is solved
Q12 = dlyap(A,B,C); 
M1 = dlyap(A,A1,C_1);
M2 = dlyap(A_2,B,C_2);

M1M1 = det(M1)^2;
M2M2 = det(M2)^2;

BC_Det_Kernel = det(Q12)^2;
BCnormalized = BC_Det_Kernel / (sqrt(M1M1)*sqrt(M2M2)); %This returns the 
%normalized value for the Determinant kernel, with a value between 0(very
%different) and 1(identical)
