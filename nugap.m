function [nu_gap] = nugap(sys1, sys2) %this function calculates the v-gap between two systems.

P1 = tf(sys1);
P2 = tf(sys2);

[~, nu_gap] = gapmetric(P1,P2);

