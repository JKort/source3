import numpy as np
import control.matlab as ml
import control
import matplotlib.pyplot as plt

class structtype():
    pass

def BC(A1, A2, C1, C2, lamda): #  TODO: Test if BC works
    C = np.transpose(C1) * C2
    C_1 = np.transpose(C1) * C1
    C_2 = np.transpose(C2) * C2
    A = lamda * np.transpose(A1)
    A_2 = lamda * np.transpose(A2)

    # The Lyapunov equation Q = lambda*A1'*Q*A2 + C1'*C2 is solved
    Q12 = control.dlyap(A, np.transpose(A2), C)
    M1 = control.dlyap(A,np.transpose(A1),C_1)
    M2 = control.dlyap(A_2, np.transpose(A2), C_2)
    M1M1 = np.linalg.det(M1)**2
    M2M2 = np.linalg.det(M2)**2
    BC_Det_Kernel = np.linalg.det(Q12)**2
    return BC_Det_Kernel / (np.sqrt(M1M1)*np.sqrt(M2M2))

order = 2
in_ = 1
out_ = 1
total_number_of_systems = 100

System = [structtype() for i in range(total_number_of_systems)]

for i in range(total_number_of_systems):
    System[i].ss = ml.drss(order, in_, out_)
    System[i].BC_distance = np.zeros([total_number_of_systems])

# System[0].A = np.array([[0, 1], [-0.2, -0.4]])
# System[1].A = np.array([[0, 1], [-0.4, -0.73]])
# System[0].C = np.array([[0, 1]])
# System[1].C = np.array([[0, 1]])

# for i in range(total_number_of_systems):
#     for j in range(total_number_of_systems):
#         System[i].BC_distance[j] = BC(System[i].A,System[j].A,System[i].C, System[j].C, lamda=0.7)

for i in range(total_number_of_systems):
    for j in range(total_number_of_systems):
        System[i].BC_distance[j] = BC(System[i].ss.A,System[j].ss.A,System[i].ss.C, System[j].ss.C, lamda=0.8)

plt.plot(System[15].BC_distance)
plt.plot(System[20].BC_distance)
plt.show()