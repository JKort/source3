import numpy as np
import scipy


def moesp(y, u, n):
    d = n+1
    ndat,ny = y.shape
    mdat,nu = u.shape
    if ndat != mdat:
        raise Exception('Y and U have different length')

    #  Block Hankel Matrix
    N = ndat - d + 1
    Y = np.zeros([d * ny, N])
    U = np.zeros([d * nu, N])
    sN = np.sqrt(N)
    sy = y.T /sN
    su = u.T /sN

    for s in range(d):
        Y[s*ny:(s+1)*ny, :] = sy[:, s:s+N]
        U[s*nu:(s+1)*nu, :] = su[:, s:s+N]

    #  LQ Decomposition
    UY = np.transpose(np.block([[U], [Y]]))
    #  qr = scipy.linalg.qr(np.transpose(np.block([[U], [Y]])))


    dgerqf = scipy.linalg.get_lapack_funcs('geqrf', (UY,))
    qr3 = dgerqf(UY)
    R = np.transpose(np.triu(qr3[0]))
    # R = np.transpose(R)
    R = R[0:d*(ny+nu), :]

    #  SVD
    R22 = R[d*nu:, d*nu:]
    U1, S1, vh = np.linalg.svd(R22)

    ss = np.diag(S1)  #  Singular value

    #  Compute C and A
    Ok = U1[:, :n]*np.diag(np.sqrt(ss[:n]))
    C = Ok[:ny, :]
    A = np.linalg.solve(Ok[:ny*(d-1), :], Ok[ny:d*ny, :])

    # Compute B and D
    L1 = np.transpose(U1[:,n:])
    R11 = R[:d*nu, :d*nu]
    R21 = R[d*nu:, :d*nu]
    M1 = L1 @ R21 @ np.linalg.inv(R11)
    m = ny*d-n
    M = np.zeros([m*d, nu])
    L = np.zeros([m*d, ny+n])

    for k in range(d):
        M[k*m:(k+1)*m, :] = M1[:, k*nu:(k+1)*nu]
        L[k*m:(k+1)*m, :] = np.block([L1[:, k*ny:(k+1)*ny], L1[:,(k+1)*ny:]@Ok[:-1-k*ny, :]])

    DB = np.linalg.solve(L, M)
    D = DB[:ny, :]
    B = DB[ny:, :]

    return A, B, C, D
