n=0.2;
m=0.25;

for k = 1:10
a = [0 1; -n*k -m*k];
System(k).a = a;
System(k).b = [0 ; 1];
System(k).c = [0.5 0.5];
System(k).d = 0;
t = linspace(0,10,101);
System(k).u = 5*sin(0.3*t); %dividing the input signal into 'sub' equal parts
[System(k).A, System(k).B, System(k).C, System(k).D, System(k).y] = ...
            systemsim(System(k).a,System(k).b,System(k).c,System(k).d,System(k).u);
Eig1(:,k) = eig(a);
Eig2(:,k) = eig(System(k).A);
end

plot(System(1).u)
hold on
plot(System(1).y)